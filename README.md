# F L I G H T B O O K E R | challenge

Challenge: Constraints.
Flight Booker is directly inspired by the Flight Booking Java example in Sodium with the simplification of using textfields for date input instead of specialized date picking widgets as the focus of Flight Booker is not on specialized/custom widgets.
[DOWNLOAD APK](https://drive.google.com/file/d/1zGbIuPcD_Q4nBcrqTZr0xHmwQawiDFDd/view?usp=sharing)

### SCREENSHOTS
-----------

![init-view](screenshots/flight_booker_i.jpeg "Main-view application")

### LANGUAGES, LIBRARIES AND TOOLS USED

* [Kotlin](https://kotlinlang.org/)
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)
* [ViewBinding](https://developer.android.com/topic/libraries/view-binding?hl=es-419)
* [ConstraintLayouts](https://developer.android.com/training/constraint-layout)

### REQUIREMENTS

* JDK 1.8
* [Android SDK](https://developer.android.com/studio/index.html)
* [Kotlin Version](https://kotlinlang.org/docs/releases.html)
* [MIN SDK 23](https://developer.android.com/preview/api-overview.html))
* [Build Tools Version 30.0.3](https://developer.android.com/studio/releases/build-tools)
* Latest Android SDK Tools and build tools.

### ARCHITECTURE

- N/A Just activity and view.

## L I C E N S E

Under the [MIT license](https://opensource.org/licenses/MIT).

