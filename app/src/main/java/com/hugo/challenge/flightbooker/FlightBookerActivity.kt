package com.hugo.challenge.flightbooker

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.hugo.challenge.flightbooker.databinding.ActivityFlightBookerBinding
import java.util.Date

class FlightBookerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFlightBookerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlightBookerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        
        loadFeature()
    }

    private fun loadFeature() {
        var checkIn = ""
        var checkOut = ""
        binding.textViewCheckIn.setOnClickListener {
            var checkInDate: List<String> = DateUtils.getCurrentDate().split("/")
            val checkInCalendar = DatePickerDialog(this,
                R.style.DatePickerDialogTheme,
                { datePicker, i, i1, i2 ->
                    checkIn = DateUtils.setCalendarForValidation(i, i1, i2)
                    if (checkOut.isNotEmpty()) {
                        if (
                            (checkOut.split(".")[2] >= checkIn.split(".")[2]) &&
                                (checkOut.split(".")[1] >= checkIn.split(".")[1]) &&
                                    (checkOut.split(".")[0] > checkIn.split(".")[0])
                        ) {
                            binding.textViewCheckIn.text = DateUtils.setCalendarFormat(i, i1, i2)
                        } else {
                            Toast.makeText(
                                this,
                                "Please, select a valid check-in date",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        binding.textViewCheckIn.text = DateUtils.setCalendarFormat(i, i1, i2)
                    }
                    checkInDate = DateUtils.setCalendarFormat(i, i1, i2).split("/")
                }, checkInDate[2].toInt(), checkInDate[1].toInt() - 1,
                checkInDate[0].toInt()
            )
            checkInCalendar.datePicker.minDate = Date().time
            checkInCalendar.show()
        }

        var isCheckoutEnabled = false

        binding.textViewCheckOut.setOnClickListener {
            if (isCheckoutEnabled) {
                var checkOutDate: List<String> = DateUtils.getCurrentDate().split("/")
                val checkOutCalendar = DatePickerDialog(this,
                    R.style.DatePickerDialogTheme,
                    { datePicker, i, i1, i2 ->
                        checkOut = DateUtils.setCalendarForValidation(i, i1, i2)
                        if (checkIn.isNotEmpty()) {
                            if (
                                (checkIn.split(".")[2] <= checkOut.split(".")[2]) &&
                                    (checkIn.split(".")[1] <= checkOut.split(".")[1]) &&
                                        (checkIn.split(".")[0] < checkOut.split(".")[0])
                            ) {
                                binding.textViewCheckOut.text = DateUtils.setCalendarFormat(i, i1, i2)
                            } else {
                                Toast.makeText(
                                    this,
                                    "Please, select a valid check-out date",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else {
                            binding.textViewCheckOut.text = DateUtils.setCalendarFormat(i, i1, i2)
                        }
                        checkOutDate = DateUtils.setCalendarFormat(i, i1, i2).split("/")
                    }, checkOutDate[2].toInt(), checkOutDate[1].toInt() - 1,
                    checkOutDate[0].toInt()
                )
                checkOutCalendar.datePicker.minDate = Date().time
                checkOutCalendar.show()
            }
        }
        
        binding.radioButtonOneWay.setOnCheckedChangeListener { buttonView, isChecked ->  
            if (isChecked) {
                isCheckoutEnabled = false
                buttonView.isChecked = true
                buttonView.background = ContextCompat
                    .getDrawable(this, R.drawable.button_selected_style)
                binding.radioButtonReturn.isChecked = false
                binding.textViewCheckOut.background = ContextCompat
                    .getDrawable(this, R.drawable.input_disabled_style)
            } else {
                buttonView.background = ContextCompat
                    .getDrawable(this, R.drawable.button_unselected_style)
            }
        }

        binding.radioButtonReturn.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                isCheckoutEnabled = true
                buttonView.isChecked = true
                buttonView.background = ContextCompat
                    .getDrawable(this, R.drawable.button_selected_style)
                binding.radioButtonOneWay.isChecked = false
                binding.textViewCheckOut.background = ContextCompat
                    .getDrawable(this, R.drawable.input_available_style)
            } else {
                buttonView.background = ContextCompat
                    .getDrawable(this, R.drawable.button_unselected_style)
            }
        }

        binding.buttonBooking.setOnClickListener {
            if (isCheckoutEnabled &&
                    binding.textViewCheckOut.text ==
                        resources.getString(R.string.form_check_out_input)) {
                Toast.makeText(
                    this,
                    "Please, select a checkout date",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                var bookedSelected = ""
                var range = ""
                if (isCheckoutEnabled) {
                    range = "from ${binding.textViewCheckIn.text} to ${binding.textViewCheckOut.text}"
                    bookedSelected = "return flight"
                } else {
                    bookedSelected = "one-way flight"
                    range = "for ${binding.textViewCheckIn.text}"
                }
                Toast.makeText(
                    this,
                    "You have booked a $bookedSelected $range",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    }
}