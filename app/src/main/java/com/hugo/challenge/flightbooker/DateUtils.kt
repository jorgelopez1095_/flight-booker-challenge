package com.hugo.challenge.flightbooker

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.GregorianCalendar

object DateUtils {

    // <><><><><><><><><><><>
    // DATE / TIME FUNCTIONS
    // <><><><><><><><><><><>

    //Get calendar instance.
    private val mCalendar = Calendar.getInstance()

    //return: CURRENT_DAY/CURRENT_MONTH/CURRENT_YEAR
    @SuppressLint("SimpleDateFormat")
    fun getCurrentDate(): String {
        return SimpleDateFormat("dd/MM/yyyy").format(mCalendar.time)
    }
    
    //Before: return $DAY de $MONTH del $YEAR
    //After: $DAYth of $MONTH, $YEAR
    @SuppressLint("SimpleDateFormat")
    fun setCalendarFormat(item: Int, item1: Int, item2: Int): String {
        val mDate = SimpleDateFormat("dd/MM/yyyy").format(
            GregorianCalendar(
                item,
                item1,
                item2
            ).timeInMillis
        ).split("/")
        return "${mDate[0]}th of ${getMonth(Integer.parseInt(mDate[1]) - 1)}, ${mDate[2]}"
    }
    
    private fun getMonth(monthValue: Int): String {
        val months = arrayOf(
            "January", "February", "March", "April",
            "May", "June", "July", "August",
            "September", "October", "November", "December"
        )
        return months[monthValue]
    }

    //return: DAY.MONTH.YEAR
    @SuppressLint("SimpleDateFormat")
    fun setCalendarForValidation(item: Int, item1: Int, item2: Int): String {
        return SimpleDateFormat("dd.MM.yyyy").format(
            GregorianCalendar(
                item,
                item1,
                item2
            ).timeInMillis
        )
    }   
    
}